public class IntegerOrderedList extends OrderedList {

    // Function comparing two integers. Returns -1 if obj1 < obj2, 0 if obj1 == obj2 and 1 if obj1 > obj2
    protected int compare(Object obj1, Object obj2){
        int int1 = ((Integer)obj1).intValue(); // Cast Object to Integer
        int int2 = ((Integer)obj2).intValue();
        if (int1 < int2) return -1; // Comparison logic
        else if (int1 == int2) return 0;
        else return 1;
    }

    // constructors
    public IntegerOrderedList () {
        name = "Integer Ordered List";
    }
    public IntegerOrderedList (String listName) {
        super(listName);
    }
}