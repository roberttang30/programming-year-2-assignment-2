public class StringOrderedList extends OrderedList {

    // Function comparing two integers. Returns -1 if obj1 < obj2, 0 if obj1 == obj2 and 1 if obj1 > obj2
    protected int compare(Object obj1, Object obj2){
        return ((String)obj1).compareTo((String)obj2); // Cast to String and compare
    }

    // constructors
    public StringOrderedList () {
        name = "String Ordered List";
    }
    public StringOrderedList (String listName){
        super(listName);
    }
}