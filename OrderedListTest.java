import java.util.Scanner;
import java.util.InputMismatchException;

public class OrderedListTest {
    public static void main(String[] args) {
        StringOrderedList StringList = new StringOrderedList("Example Ordered List of Strings"); // Create lists and give them names
        IntegerOrderedList IntList = new IntegerOrderedList("Example Ordered List of Integers");

        Scanner userInput = new Scanner(System.in); // Used to get input from user

        while (true){ // Keep going until the user exits
            try{ // Check for erroneous inputs
                System.out.println("Would you like to access the StringOrderedList test, the IntegerOrderedList test or exit the programme? (1, 2 or 3)");
                int list = userInput.nextInt(); // Get user response

                OrderedList ListObject; // We will use the same object to store whichever list we are currently dealing with. The type OrderedList is used to handle both Integer and String
                if (list == 1) ListObject = StringList;
                else if (list == 2) ListObject = IntList;
                else if (list == 3) return; // Exit
                else throw new InputMismatchException(); // No other values are valid, error

                while (true) { // Keep dealing with the same list until we are told otherwise
                    try{
                        System.out.println("Would you like to insert an element into the list, remove an element, print the list or choose another list? (1, 2, 3 or 4)");
                        int mode = userInput.nextInt();
                        if (mode == 1 || mode == 2){ // Adding and removing to the list are dealth with together due to the similar logic involved
                            System.out.printf("Please enter the data you would like to %s the list then press enter.%n",(mode == 1) ? "add to":"remove from"); // Present an appropriate message depending on the mode we are in
                            Object data = null; // Same variable whicher tye of data we are dealing with (object is a superclass of String and Integer)
                            if (list == 1 ) data = userInput.next(); // User input function appropriate to data type
                            else if (list == 2) data = userInput.nextInt();
                            if (mode == 1) { // We are adding to the list
                                ListObject.insert(data);
                                System.out.println("Data inserted");
                            }
                            else { // Otherwise we must be remvoing data
                                System.out.println(ListObject.remove(data) ? "Data removed" : "Data not found"); // Remove data and print a message indicating the result
                            }
                        }
                        else if (mode == 3) {
                            System.out.println(ListObject); // Print the list invoking the toString method
                        }
                        else if (mode == 4) { // Return to list selection
                            break;
                        }
                        else throw new InputMismatchException(); // No other values are valid, error
                    }
                    catch (InputMismatchException e){
                        System.out.println("Input invalid, please try again. n.b Menu options must be entered as integers"); // Print a descriptive error message
                        userInput.nextLine(); // Consume the delimiter at the end of the line which was missed due to the error.
                    }
                }
            }
            catch (InputMismatchException e){
                System.out.println("Input invalid, please try again. Input must be 1, 2 or 3"); // Print a descriptive error message
                userInput.nextLine(); // Consume the delimiter at the end of the line which was missed due to the error.
            }
        }
    }
}