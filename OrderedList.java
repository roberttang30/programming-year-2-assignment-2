public abstract class OrderedList extends List {

    // Abstract function comparing two objects. Returns -1 if obj1 < obj2, 0 if obj1 == obj2 and 1 if obj1 > obj2
    protected abstract int compare(Object obj1, Object obj2); 

    // Function which adds a node to the list at the correct point according to its value
    public void insert (Object newData) { 
        ListNode newNode = new ListNode(newData, null); // Create a node with the given data
        if (firstNode == null) // empty list
            firstNode = lastNode = newNode; // newNode is the only node so it is the first and the last
        else {
            ListNode currentNode = firstNode; // Start at the beggining of the list
            if (compare(newNode.getData(), firstNode.getData()) < 0){ // Check if the newNode is less than the firstNode
                newNode.setNext(firstNode); 
                firstNode = newNode;
                return;
            }
            while (true) {
                ListNode nextNode = currentNode.getNext(); // Next node in the list
                if (compare(newNode.getData(), currentNode.getData()) == 0) return;
                else if (nextNode == null){ // If we are at the end of the list
                    currentNode.setNext(newNode);
                    lastNode = newNode; // newNode becomes the last node
                    return;
                }
                else if (compare(newNode.getData(), nextNode.getData()) < 0){ // Check of newNode is less than the following node, if so this is its position in the list
                    newNode.setNext(nextNode);
                    currentNode.setNext(newNode);
                    return;
                }
                currentNode = nextNode; // Move along in the list
            }
        }
    }

    public Boolean remove (Object remData) {
        if (firstNode == null) return false; // If the list is empty, return
        if (compare(firstNode.getData(), remData) == 0){ // Check if the node to be removed is the first node
            firstNode = firstNode.getNext(); // Update firstNode
            if (firstNode == null) lastNode = null; // Handle empty list
            return true; // Object was found and removed
        }
        ListNode currentNode = firstNode; //Start at the beggining of the list
        while (true) {
            ListNode nextNode = currentNode.getNext(); // Next node in the list
            if (nextNode == null) return false; // Reached end of list
            if (compare(nextNode.getData(), remData) == 0) { // Found it
                ListNode nextNextNode = nextNode.getNext();
                currentNode.setNext(nextNextNode); // Update references
                if (nextNextNode == null) lastNode = currentNode; // End of list
                return true; // Object was found and removed
            }
            currentNode = nextNode; // Move along in the list
        }
    }

    public void insertAtFront (Object newData){ // Override unwanted method
        throw new RuntimeException("Method unavaiable, use .insert() instead");
    }

    public void insertAtBack (Object newData){ // Override unwanted method
        throw new RuntimeException("Method unavaiable, use .insert() instead");
    }

    // constructors
    public OrderedList () {
        name = "Ordered List";
    }

    public OrderedList (String listName){
        super(listName);
    }
}