public class List{
    protected ListNode firstNode;  // the first node
    protected ListNode lastNode;  // the last node
    protected String name;  // a string name

    public void insertAtFront (Object newData){ // insert Object at front
        if (firstNode == null) // empty list
            firstNode = lastNode = new ListNode(newData, null);
        else {
            ListNode newFirstNode = new ListNode(newData, firstNode);
            firstNode = newFirstNode;
        }
    }

    public void insertAtBack (Object newData){ // insert Object at back
        if (firstNode == null) // empty list
            firstNode = lastNode = new ListNode(newData, null);
        else {
            ListNode newLastNode = new ListNode(newData, null);
            lastNode.setNext(newLastNode);
            lastNode = newLastNode;
        }
    }

    public Object removeFromFront (){ // remove Object from front
        if (firstNode == null) // empty list
            return null;
        Object removedData = firstNode.getData();
        if (firstNode == lastNode) // only one list node
            firstNode = lastNode = null;
        else
            firstNode = firstNode.getNext();
        return removedData;
    }

    public Object removeFromBack (){ // remove object from back
        if (firstNode == null) // empty list
            return null;
        Object removedData = lastNode.getData();
        if (firstNode == lastNode) // only one node in the list
            firstNode = lastNode = null;
        else {
            ListNode current = firstNode;
            while (current.getNext() != lastNode)
                current = current.getNext();
            lastNode = current;
            current.setNext(null);
        }

        return removedData;
    }

    // this allows us to get a "handle" to navigate through the list in a loop
    public ListNode getFirst () {
        return firstNode;
    }

    public String toString () { // print list content to string
        String output = new String();
        ListNode current = firstNode;
        output = name + ":";
        while (current != null) {
            // we are implicitly calling the data object toString method
            output += " " + current.getData();
            current = current.getNext();
        }
        return output;
    }

    // constructors
    public List (String listName) { 
        firstNode = lastNode = null; name = listName;
    }
    
    public List () {
        this("List");
    }

}